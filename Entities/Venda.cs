using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public EnumStatusVenda StatusVenda { get; set; }
        public DateTime Data { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
        
    }
}