using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Enums;


namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendaTest = _context.Vendas.Find(id);
            
            if (vendaTest is null) {
                return NotFound();
            }

            var venda = _context.Vendas.Where(x => x.Id == id)
                .Include(v => v.Vendedor).Include(i => i.Itens);

            return Ok(venda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda([Required] int IdVendedor, [Required] List<int> IdItens)
        {
            Venda venda = new Venda();
            var vendedor = _context.Vendedores.Find(IdVendedor);

            if (vendedor is null)
            {
                return NotFound("Vendedor não encontrado.");
            }

            if (IdItens is null || IdItens.Count == 0) {
                return BadRequest(new { Erro = "Uma venda precisa de pelo menos 1 item" });
            }

            venda.StatusVenda = EnumStatusVenda.AguardandoPagamento;
            venda.Vendedor = vendedor;
            venda.Data = DateTime.Now;
            venda.Itens = new List<Item>();

            foreach(int i in IdItens) {

                var item = _context.Itens.Find(i);

                if (item is null) {
                    return BadRequest(new { Erro = $"Id de item inválido: {i}" });
                }

                venda.Itens.Add(item);
            }

            if (venda.Data == DateTime.MinValue) {
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });
            }

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPatch("{id}")]
        public IActionResult AtualizarStatus(int id, EnumStatusVenda status)
        {
            var vendaTest = _context.Vendas.Find(id);

            if (vendaTest == null) {
                return NotFound();
            }

            if(StatusTeste(vendaTest.StatusVenda, status))
            {
                vendaTest.StatusVenda = status;

                _context.Vendas.Update(vendaTest);
                _context.SaveChanges();

                var venda = _context.Vendas.Where(x => x.Id == id)
                .Include(v => v.Vendedor).Include(i => i.Itens);

                return Ok(venda);
            }
            else
            {
                return BadRequest("Transição de Status Inválida!");
            }
        }

        private bool StatusTeste(EnumStatusVenda StatusAnterior, EnumStatusVenda StatusNovo)
        {
            var ap = EnumStatusVenda.AguardandoPagamento;
            var pa = EnumStatusVenda.PagamentoAprovado;
            var ept = EnumStatusVenda.EnviadoParaTransportadora;
            var e = EnumStatusVenda.Entregue;
            var c = EnumStatusVenda.Cancelado;

            if (StatusAnterior == ap && (StatusNovo == pa || StatusNovo == c)) return true;

            else if (StatusAnterior == pa && (StatusNovo == ept || StatusNovo == c)) return true;

            else if (StatusAnterior == ept && StatusNovo == e) return true;

            return false;
        }
    }
}