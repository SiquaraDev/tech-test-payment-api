using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;


namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendedorController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendedores = _context.Vendedores;
            return Ok(vendedores);
        }

        [HttpPost("GerarVendedores")]
        public IActionResult GerarVendedores()
        {
            AdicionarVendedor("Matheus", "15675895432", "matheus@email.com", "(21) 98654-4325");
            AdicionarVendedor("Siquara", "85624519635", "siquara@mail.com", "(11) 98541-7726");

            return Ok("Dois vendedores foram gerados.");
        }

        [HttpPost]
        private void AdicionarVendedor([Required] string Nome, [Required] string Cpf, [Required][EmailAddress] string Email, [Required] string Telefone)
        {
            Vendedor vendedor = new Vendedor();

            vendedor.Nome = Nome;
            vendedor.CPF = Cpf;
            vendedor.Email = Email;
            vendedor.Telefone = Telefone;

            _context.Add(vendedor);
            _context.SaveChanges();
        }
    }
}