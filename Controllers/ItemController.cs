using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly PaymentContext _context;

        public ItemController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var itens = _context.Itens;
            return Ok(itens);
        }

        [HttpPost("GerarItens")]
        public IActionResult GerarItens()
        {
            AdicionarItem("Camisa", 35);
            AdicionarItem("Moletom", 50);
            AdicionarItem("Calca", 90);
            AdicionarItem("Cueca", 10);
            AdicionarItem("Cinto", 40);

            return Ok("Cinco itens foram gerados.");
        }

        [HttpPost]
        private void AdicionarItem([Required] string Nome, [Required] decimal Valor)
        {
            Item item = new Item();

            item.Nome = Nome;
            item.Valor = Valor;

            _context.Add(item);
            _context.SaveChanges();
        }
    }
}